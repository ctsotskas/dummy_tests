package dummy_tests;

import java.util.ArrayList;
import java.util.List;


public class Collection {

	private ArrayList<Integer> marks;



	private double calculateAverage() {
		Integer sum = 0;
		if(!marks.isEmpty()) {
			for (Integer mark : marks) {
				sum += mark;
			}
			return sum.doubleValue() / marks.size();
		}
		return sum;
	}

	public Collection(final int numberOfElements){
		marks = new ArrayList<Integer>();		
	}
	
	public Collection(List <Integer> externalList){
		marks = new ArrayList<Integer>(externalList);
	}
}
