package tests;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Set;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import dummy_tests.SimpleMaths;

public class TestSimpleMaths {

	public static SimpleMaths tester;

	@BeforeClass
	public static void testSetup() {
		tester = new SimpleMaths();
	}
	//
	// @AfterClass
	// public static void testCleanup() {
	// // Do your cleanup here like close URL connection , releasing resources
	// etc
	// }
	//
	// @Test(expected = IllegalArgumentException.class)
	// public void testExceptionIsThrown() {
	// tester.divide(1000, 0);
	// }

	@Test
	public void testSizeOfList() {
		Set<Integer> test_list1 = tester.generateAnArrayOfIntegers(4);
		assertEquals("Total number of elements required", 4, test_list1.size());
	}

}